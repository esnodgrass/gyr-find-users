package org.getyourrefund;

import io.cucumber.java.PendingException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class FindUserSteps {

    private final WebDriver driver = CukeSetup.getDriver();

    private void checkForUser(String searchText, boolean islastPage) {
        WebElement tableBody = driver.findElement(By.className("index-table__body"));
        List<WebElement> userRows = tableBody.findElements(By.cssSelector("tr.index-table__row"));

        for (WebElement userRow : userRows) {
            try {
                WebElement nameLink = userRow.findElement(By.cssSelector("th a"));
                if (nameLink.getText().contains(searchText)) {

                    System.out.println(nameLink.getAttribute("href"));
                }

            } catch (Exception e) {
                // do nothing
            }
        }
        if (!islastPage) {
            driver.findElement(By.cssSelector("a.next_page")).click();
            checkForUser(searchText, nextButtonIsDisabled());
        }
    }

    private boolean nextButtonIsDisabled() {
        boolean isDisabled;
        try {
            driver.findElement(By.cssSelector("span.next_page.disabled"));
            isDisabled = true;
        } catch (Exception e) {
            isDisabled = false;
        }
        return isDisabled;
    }

    @When("they search for {string}")
    public void theySearchFor(String searchString) {
        boolean isLastPage = nextButtonIsDisabled();
        checkForUser(searchString, isLastPage);
    }

    @Then("the page number should be returned")
    public void thePageNumberShouldBeReturned() {
        throw new PendingException();
    }

    @Given("{word} logs in using password {word}")
    public void userLogsInUsingPassword(String username, String password) {
//        driver.get("https://getyourrefund.org/en/hub/sign_in");
        driver.get("https://demo.getyourrefund.org/en/hub/sign_in");
        driver.findElement(By.id("user_email")).sendKeys(username);
        driver.findElement(By.id("user_password")).sendKeys(password);
        driver.findElement(By.id("new_user")).submit();
    }

    @And("they are looking for users")
    public void theyAreLookingForUsers() {
        driver.findElement(By.xpath("//a[contains(@href,'/en/hub/profile')]")).click();
        driver.findElement(By.xpath("//a[@href='/en/hub/users']")).click();
    }
}
