Feature: Find a user by name
  Scenario: Search through user pages til found
    Given <username> logs in using password <password>
    And they are looking for users
    When they search for "Vaughn"
    Then the page number should be returned