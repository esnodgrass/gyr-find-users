# README #

### What is this repository for? ###

This project is **NOT** an example of how to use gherkin, bdd, or testing. 
These tools were used to solve a problem in a pinch.

### What is this repository for? ###

This is a work-around for the lack of search capability on the users page within the GYR hub.

### How do I get set up? ###

* In the **find_user.feature** file you will need to update with your email address and password.
* In the **FindUserSteps.java** file you can switch from `demo.getyourrefund.org` 
  to `getyourrefund.org` in the `userLogsInUsingPassword` method.

### Running the feature

* I typically use IntelliJ IDEA with the Cucumber Java Plug-In and run from the run icons in the feature file
* To run from the command line 
  * You must have `gradle` installed
  * From the project directory run `gradle cucumber`